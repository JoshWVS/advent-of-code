/^forward/ {
    horiz_pos += $2
    vert_pos += aim * $2
}

/^down/ {
    aim += $2
}

/^up/{
    aim -= $2
}

END {
    print "Final horizontal position:", horiz_pos
    print "Final depth:", vert_pos
    print "Final aim:", aim
    print "Answer:", horiz_pos * vert_pos
}
