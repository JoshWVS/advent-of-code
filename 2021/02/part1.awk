/^forward/ {
    horiz_pos += $2
}

/^down/ {
    vert_pos += $2
}

/^up/{
    vert_pos -= $2
}

END {
    print "Final horizontal position:", horiz_pos
    print "Final depth:", vert_pos
    print "Answer:", horiz_pos * vert_pos
}
