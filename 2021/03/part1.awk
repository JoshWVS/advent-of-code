# Treat each character as a separate field
# Note: not supported by all versions of awk (works in gawk)
BEGIN { FS = "" }

# The i-th index of arr tracks how many times we've seen a 1 in the position
# corresponding to 2^i. E.g., arr[0] tracks how many times we've seen a 1 as
# the last digit of the number (since the last digit always has associated
# power 2^0 = 1). (Choosing this order makes it easier to reconstruct the final
# answers later.)
{
    for (i = 1; i <= NF; i++) {
        arr[NF-i] += $i
    }
}

END {
    # Convert our binary data to decimal
    for (i in arr) {
        # Find which bit occurred most often
        gamma_bit = arr[i] >= (NR / 2) ? 1 : 0
        gamma += 2^i * gamma_bit
        # To get epsilon, just invert the bits of gamma
        epsilon += 2^i * (1 - gamma_bit)
    }

    print "Gamma:", gamma
    print "Epsilon:", epsilon
    print "Product:", gamma * epsilon
}
