#lang racket
(module+ test
  (require rackunit)
  (define readings (map parse-line (file->lines "input.txt"))))

; Convert a line to a vector of 1s and 0s (as integers)
(define (parse-line line)
  (list->vector (map
                  (lambda (x) (- (char->integer x) 48))
                  (string->list line))))

; Convert a list of bits (most significant first) to an integer
(define (binary-to-decimal lst)
  (foldl (lambda (x acc) (+ x (* 2 acc))) 0 lst))

; Given a list of bit vectors (lst), find the most common bit at the given
; index (idx)
(define (most-common-bit lst idx)
  (let*
    [(idx-bits (map (lambda (x) (vector-ref x idx)) lst))
     (ones-count (apply + idx-bits))]
    (if (>= ones-count (/ (length lst) 2)) 1 0)))

(define (least-common-bit lst idx)
  (- 1 (most-common-bit lst idx)))

; Helper function for filtering
(define (matches-bit-at? vec bit idx)
  (equal? (vector-ref vec idx) bit))

(define (calc-life-support-helper o-candidates co2-candidates idx)
  (let*
    [(found-o-rating (equal? (length o-candidates) 1))
     (found-co2-rating (equal? (length co2-candidates) 1))]
    (if (and found-o-rating found-co2-rating)
        (list (first o-candidates) (first co2-candidates)) ; Base case--we're done
        (let*
          [(mc-o-bit (most-common-bit o-candidates idx))
           (lc-co2-bit (least-common-bit co2-candidates idx))
           (keep-o-candidate? (lambda (x) (matches-bit-at? x mc-o-bit idx)))
           (keep-co2-candidate? (lambda (x) (matches-bit-at? x lc-co2-bit idx)))]
          (cond ; Otherwise, recurse on either/both lists as needed
            [found-o-rating
             (calc-life-support-helper o-candidates (filter keep-co2-candidate? co2-candidates) (+ idx 1))]
            [found-co2-rating
             (calc-life-support-helper (filter keep-o-candidate? o-candidates) co2-candidates (+ idx 1))]
            [else
              (calc-life-support-helper
                (filter keep-o-candidate? o-candidates)
                (filter keep-co2-candidate? co2-candidates)
                (+ idx 1))])))))

; Main entry point: sets up call to helper function, and converts the return
; value to the answer format expected for the puzzle
(define (calc-life-support readings)
  (let*
    [(mc-bit (most-common-bit readings 0))
     (has-leading-mc-bit? (lambda (x) (matches-bit-at? x mc-bit 0)))
     (o-candidates   (filter     has-leading-mc-bit? readings))
     (co2-candidates (filter-not has-leading-mc-bit? readings))
     (raw-answer (calc-life-support-helper o-candidates co2-candidates 1))
     (decimal-answer (map (lambda (x) (binary-to-decimal (vector->list x))) raw-answer))]
    (apply * decimal-answer)))

(module+ test
  (check-equal? 2784375 (calc-life-support readings)))
