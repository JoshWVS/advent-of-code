#lang racket
(module+ test
  (require rackunit)
  (define input (map string->number (file->lines "input.txt"))))

(define (count-increasing-sums-helper window lst acc)
  (cond
    [(empty? lst) acc]
    [else (let*
            ([old-window-sum (apply + window)]
             [new-window (cons (first lst) (drop-right window 1))]
             [new-window-sum (apply + new-window)]
             [incr (if (> new-window-sum old-window-sum) 1 0)])
            (count-increasing-sums-helper new-window (rest lst) (+ acc incr)))]))

(define (count-increasing-sums lst [window 1])
    (when (>= window (length lst)) (error "window must be < length(lst)"))
    (let-values
      ([(initial-window initial-lst) (split-at lst window)])
      (count-increasing-sums-helper (reverse initial-window) initial-lst 0)))

(module+ test
  (check-equal? 1292 (count-increasing-sums input 1))  ; Part 1
  (check-equal? 1262 (count-increasing-sums input 3))) ; Part 2

(provide (contract-out
           [count-increasing-sums
             (->*
               [(listof natural-number/c)]
               [natural-number/c]
               natural-number/c)]))
