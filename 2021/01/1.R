window_apply <- function(X, FUN, window = 1) {
    last_idx <- length(X) - window
    windows <- lapply(0:last_idx,
                      function (y) (1:window) + y)

    lapply(windows, function(window) FUN(X[window]))
}

count_increasing_sums <- function(x, window = 1) {
    window_sums <- unlist(window_apply(x, sum, window))
    sum((window_sums[-1] - window_sums[1:length(window_sums)-1]) > 0)
} 

# To run for Advent of Code:
# source("1.R")
# v <- read.table("input.txt")[[1]]
# count_increasing_sums(v, 1) # part 1
# count_increasing_sums(v, 3) # part 2
