NR==1 { prev = $0 }

NR > 1 {
    if ($0 > prev) { count += 1;}
    prev = $0
}

END { print count }
