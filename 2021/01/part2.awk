# Could generalize this to a window of size k provided as a command-line
# argument...
NR==1 { prev3 = $0 }
NR==2 { prev2 = $0 }
NR==3 { prev1 = $0 }

NR > 3 {
    prev_sum = prev3 + prev2 + prev1
    if ($0 + prev1 + prev2 > prev_sum) { count += 1; }

    prev3 = prev2
    prev2 = prev1
    prev1 = $0
}

END { print count }
