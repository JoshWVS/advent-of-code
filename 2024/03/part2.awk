BEGIN {
    MUL_PAT = "mul\\(([[:digit:]]+),([[:digit:]]+)\\)"
    DO_PAT = "do\\(\\)"
    DONT_PAT = "don't\\(\\)"
    FPAT = MUL_PAT "|" DO_PAT "|" DONT_PAT
    active = 1
}

{
    for (i = 1; i <= NF; i++) {
        if (match($i, MUL_PAT, matches)) {
            total += active * matches[1] * matches[2]
            continue
        }
        if ($i ~ DO_PAT) {
            active = 1
            continue
        }
        if ($i ~ DONT_PAT) {
            active = 0
            continue
        }
        print "Unrecognized input: " $i
    }
}

END {
    print total
}
