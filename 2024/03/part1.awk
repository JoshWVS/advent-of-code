BEGIN {
    FPAT = "mul\\(([[:digit:]]+),([[:digit:]]+)\\)"
}

{
    for (i = 1; i <= NF; i++) {
        match($i, FPAT, matches)
        total += matches[1] * matches[2]
    }
}

END {
    print total
}
