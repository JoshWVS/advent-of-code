BEGIN {
    FS = "|"
}

/\|/ {
    rules[$1, $2] = ">"
}

/^$/ {
    FS = ","
}

/,/ {
    valid = 1
    for (i = 1; i <= NF; i++) {
        for (j = 1; j <= i; j++) {
            if (($i, $j) in rules) {
                print "Pair (" $i, $j ") violates a rule"
                valid = 0
                break
            }
        }
        if (!valid) {
            break
        }
    }

    if (valid) {
        middle_value = $(int(NF / 2)+1)
        print $0, "It's valid and the middle number is", middle_value
        total += middle_value
    }
}


END {
    print total
}
