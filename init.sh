#!/bin/bash

# Script to prepare daily directory/inputs for Advent of Code.
# $1 (optional) - day to run for (defaults to today)
# $2 (optional) - year to run for (defaults to current year)
#
# Note: to download the input files, you must set AOC_SESSION to your Advent of
# Code session (from the Cookie header). Run this script without setting
# AOC_SESSION for more details.

DAY=${1:-$(date +%d)}
# We want leading zeros in the directory name (for proper sorting), but Advent
# of Code doesn't use them in the input URL. The expression below just converts
# the input to decimal, which drops the leading zeros.
DAY_NO_LEADING_ZERO=$((10#"${DAY}"))
YEAR=${2:-$(date +%Y)}

if [ -z "${AOC_SESSION}" ]; then
    echo "Please set AOC_SESSION to your Advent of Code session (the portion after 'session='; this is required to fetch inputs)."
    echo "(You can find this by using your browser's dev tools to see the value of the 'Cookie' header when you access your input.)"
    exit 1
fi

DIR=$(printf "${YEAR}/%02d" "${DAY}")
mkdir -p "${DIR}"
curl --silent --cookie "session=${AOC_SESSION}" "https://adventofcode.com/${YEAR}/day/${DAY_NO_LEADING_ZERO}/input" > "${DIR}/input.txt"

cat << EOF > "${DIR}/${DAY_NO_LEADING_ZERO}.rkt"
#lang racket
(module+ test
  (require rackunit)
  (define input (file->lines "input.txt")))
EOF
